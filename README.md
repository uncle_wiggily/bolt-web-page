# Signup page for Bolt drivers

This project is a realization of simple, mobile responsive Bolt signup page using HTML, CSS and React.

React is mostly used for convenient division of HTML into smaller components and also for form validations.

## Preview

The preview is available [here](http://dijkstra.cs.ttu.ee/~jupono/own/bolt-demo/).

## Preview on local machine
Clone repository
```bash
git clone https://gitlab.com/uncle_wiggily/bolt-web-page.git
```

And open index.html file in dist directory with your browser.
It should look something like this:
```
file:///[path-to-cloned-directory]/dist/index.html
```

## Preview in dev mode
Clone repository

Run (in project folder):
```bash
npm i
npm start
```

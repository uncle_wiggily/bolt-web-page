const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require("path");	

module.exports = {
	entry: ['@babel/polyfill', './src/index.js'],
	output: {
        path: path.resolve('dist'),
        filename: 'bundle.js'
    },
	module: {
		rules: [
		{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			use: {
				loader: "babel-loader"
			}
		},
		{
			test: /\.html$/,
			use: {
				loader: "html-loader"
			}
		},
		{
            test: /\.scss$/,
            use: [
                "style-loader",
                "css-loader",
                "sass-loader"
            ]
		}]
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: "./public/index.html",
			filename: "./index.html"
		})
	],
	devServer: {
		port: 3000,
		proxy: {
			'/api': {
			  	target: 'https://example-api.example.com',
			}
		}
	}
};
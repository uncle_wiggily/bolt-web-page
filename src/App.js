import React from 'react';
import './scss/base.scss';
import Main from './js';

const App = () => (
	<Main />
)

export default App;

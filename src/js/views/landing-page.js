import React from 'react';
import TopBar from '../layout/top-bar';
import DriverSignupForm from '../components/LandingPage/driver-signup-form';
import ExtendedLandingView from './extended-landing-view';

const LandingPage = () => {
	return (
		<div className="container-fluid">
			<div className="row">
				<div className="col-lg-6 d-none d-lg-block p-0">
					<ExtendedLandingView />
				</div>
				<div className="col-lg-6 p-0">
					<TopBar />
					<div className="container-fluid pt-2 pt-lg-5">
						<div className="row justify-content-center">
							<div className="col-11 col-sm-8 col-xl-7 pr-lg-0">
								<h5 className="text--weight-bold">Signup as a driver</h5>
								<p className="text--size-tiny text--weight-medium my-2">Sign up here if you are a driver who would like to use Taxify platform.</p>
								<DriverSignupForm />
								<div className="text-center">
									<p className="text--size-tiny mt-3 mb-0" style={{lineHeight: 0.1}}>Have multiple cars and drivers?</p>
									<a className="link--color-ultra-green text--size-tiny" href="#">Signup as a fleet owner</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default LandingPage;
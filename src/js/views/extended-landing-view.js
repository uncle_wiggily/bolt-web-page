import React from 'react';

const LandingPage = () => {
	return (
		<div className="extended-view">
			<img className="logo--position-top-left ml-4 mt-3"
				src="http://dijkstra.cs.ttu.ee/~jupono/own/images/bolt-website-logo.png"
				alt='Bolt Logo'
			/>
			<div className="centered-hor-vert text-white">
				<h2 className="text--weight-bold">Drive with Bolt</h2>
				<p className="text--weight-regular text--size-small">Earn good money with your car.</p>
			</div>
		</div>
	)
}

export default LandingPage;
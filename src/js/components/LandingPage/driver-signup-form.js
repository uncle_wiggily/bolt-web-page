import React from 'react';
import { useForm } from '../../common/customHooks/use-form';
import { validateSignupRequest } from '../../common/validation-rules';

const DriverSignupForm = () => {

	const submitFormRequest = () => {
		console.info("Form is valid and submitted. Call to API for example.")
	}

	const { values, errors, handleChange, handleSubmit } 
		= useForm(submitFormRequest, validateSignupRequest);

	return (
		<form noValidate>
			<div className="form-group mb-1">
				<label className="text--size-tiny text--weight-semi-bold mb-0" htmlFor="emailAddressInput">Email</label>
				<input type="email" className={`form-control form-control-sm ${errors.email && 'is-invalid'}`}
					id="emailAddressInput" name="email" value={values.email || ''} onChange={handleChange} />
				<small id="emailHelp" className="form-text text--size-xtiny text-muted mt-0">Will be used as your username</small>
				{errors.email && ( <div className="invalid-feedback">{errors.email}</div> )}
			</div>
			<div className="form-group mb-1">
				<label className="text--size-tiny text--weight-semi-bold mb-0" htmlFor="phoneNumberInput">Phone</label>
				<div className="input-group">
					<input type="tel" className="form-control form-control-sm form-control__input--country"
						placeholder="+372" id="phoneNumberInput" name="countryCode"
						value={values.countryCode || ''} onChange={handleChange}/>
					<input type="tel" className="form-control form-control-sm form-control__input--phone"
						placeholder="555-555-33" name="phoneNumber" 
						value={values.phoneNumber || ''} onChange={handleChange} />
				</div>
				{ errors.phoneNumber && ( <div className="invalid-feedback d-block">{errors.phoneNumber}</div> )}
			</div>
			<div className="form-group mt-1 mb-0">
				<label className="text--size-tiny text--weight-semi-bold mb-0" htmlFor="cityInput">City</label>
				<input type="text" className={`form-control form-control-sm ${errors.city && 'is-invalid'}`}
					id="cityInput" name="city" value={values.city || ''} onChange={handleChange} />
				{ errors.city && ( <div className="invalid-feedback">{errors.city}</div> )}
			</div>
			<div className="form-check form-check-inline mt-2 mb-3">
				<input type="checkbox" className="form-check-input" id="termsCheck" />
				<label className="form-check-label text-muted text--size-xtiny" htmlFor="termsCheck">
					By signing up, you accept our&nbsp;
					<a className="link--color-grey" href="#">Terms of Service</a>&nbsp;and&nbsp;
					<a className="link--color-grey" href="#">Privacy Policy</a>
				</label>
			</div>
			<button type="submit" onClick={handleSubmit}
				className="btn btn-block btn-ultra-green text--size-tiny text--weight-semi-bold mb-2">
				PROCEED
			</button>
		</form>		
	)
}

export default DriverSignupForm;
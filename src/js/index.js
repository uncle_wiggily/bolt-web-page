import React from 'react';
import LandingPage from './views/landing-page';

const Main = () => (
	<LandingPage />
)

export default Main;

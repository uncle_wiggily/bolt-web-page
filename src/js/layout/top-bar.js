import React from 'react';

const TopBar = () => {
	return (
		<nav className="navbar navbar-expand navbar-light pt-2 pt-lg-3 pr-2">
			<a className="navbar-brand d-lg-none" href="#">
				<img src="http://dijkstra.cs.ttu.ee/~jupono/own/images/bolt-logo-original-on-white.png" width="60" height="40" alt="Bolt Logo" />
			</a>
			<ul className="navbar-nav ml-auto">
	  			<span className="navbar-text text-weight--medium text--size-tiny d-none d-lg-inline">
				    Already have an account?
				</span>
				<button 
					className="btn btn-sm btn-long btn-grey text--weight-bold order-last ml-2 mr-2 my-auto"
				 	type="button">SIGN IN
				</button>
		      	<li className="nav-item dropdown order-lg-last">
					<a className="nav-link text-dark text--size-tiny" href="#" id="navbarLanguageDropdown" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i className="material-icons text--weight-medium mr-1">language</i><span className="d-none d-lg-inline">English</span>
					</a>
					<div className="dropdown-menu dropdown-menu-right text--size-tiny" aria-labelledby="navbarLanguageDropdown">
						<a className="dropdown-item" to="#">Estonian</a>
						<a className="dropdown-item" to="#">Russian</a>
						<a className="dropdown-item" to="#">English</a>
					</div>
				</li>
		    </ul>
		</nav>
	)
}

export default TopBar;
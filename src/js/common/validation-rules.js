// These validation rules are just an example, actual validation
// should be much more robust
export const validateSignupRequest = (values) => {
	let errors = {};

	if (!values.email) {
		errors.email = "Please provide your email so we can contact you."
	} else if (!/\S+@\S+\.\S+/.test(values.email)) {
		errors.email = "Wrong email format."
	}

	if (!values.phoneNumber || !values.countryCode || !/^\d{7,8}/.test(values.phoneNumber)) {
		errors.phoneNumber = "Please specify correct phone number and country code so we can contact you."
	}

	if (!values.city) {
		errors.city = "Please enter the city you are living in."
	} else if (!/^[a-zA-Z]+$/.test(values.city)) {
		errors.city = "Only letters and spaces are allowed in city name."
	}

	return errors;
}